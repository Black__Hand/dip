<?php
 
class Users extends _MainModel{


    public function getListUsers(){
        $result = _MainModel::table("User_person_data")->get()->send();

        _MainModel::viewJSON($result);

    }
    public function PDOUser()
    {

        $stmt = self::$db->prepare("SELECT id FROM  User_person_data WHERE email= :email AND password= :password");
        $result_query = $stmt->execute(array(":email" => _MainModel::$params_url['email'], ":password" => _MainModel::$params_url['password']));

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        _MainModel::viewJSON($rows);

    }
    public function AddUser()
    {
        $email = $_GET["email"];
        $id = $_GET["id"];
        $name = $_GET["name"];
        $surname = $_GET["surname"];
        $patronymic = $_GET["patronymic"];
        $password = $_GET["password"];
        $phone = $_GET["phone"];
        $phone_token = $_GET["Phone_token"];
        $phone_token_date = $_GET["Phone_token_date"];
        $birth_date = $_GET["date_of_birth"];
        $gender = $_GET["gender"];
        $passport = $_GET["passport"];
        $other_data = $_GET["other_data"];


        if (!isset($id) || !isset($email) || !isset($name) || !isset($surname) || !isset($patronymic)
            || !isset($password) || !isset($phone) || !isset($phone_token) || !isset($phone_token_date)
            || !isset($birth_date) || !isset($gender) || !isset($passport) || !isset($other_data))
        {
            _MainModel::viewJSON("Error, you missed _GET");
        }
        elseif ($id == "" || $email == "" || $name == "" || $surname == "" || $patronymic == ""
            || $password == "" || $phone == "" || $phone_token == "" || $phone_token_date == ""
            || $birth_date == "" || $gender == "" || $passport == "" || $other_data == "")
        {
            _MainModel::viewJSON("Error, you missed something in string ");
        }
        else
        {
            _MainModel::table("user_person_data")->add(array("id" => $id, "email" =>  $email
            ,"password" =>  $password, "phone" =>  $phone, "phone_token" =>  $phone_token,
                "phone_token_date" =>  $phone_token_date, "name" =>  $name,
                "surname" =>  $surname, "patronymic" =>  $patronymic,
                "date_of_birth" =>  $birth_date, "gender" =>  $gender,
                "passport" =>  $passport, "other_data" =>  $other_data))->send();
        }

    }
    public function editUser()
    {
        _MainModel::table("user_person_data")->edit(array("name" => "значение1", "surname" => "значение2"), array("id" => 1))->send();
    }
    public function deleteUser()
    {
        _MainModel::table("user_person_data")->delete(array("id" => 2))->send();
    }
}
















