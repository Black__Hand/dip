<?php


class users_cards extends _MainModel
{
    public function getListUsersCards(){
        $result = _MainModel::table("users_cards")->get()->send();

        _MainModel::viewJSON($result);

    }
    public function AddUserCards()
    {

        $id = $_GET["id"];
        $role = $_GET["role"];
        $id_parent = $_GET["id_parent"];
        $rating = $_GET["rating"];
        $status = $_GET["status"];


        if (!isset($id) || !isset($role) || !isset($id_parent) || !isset($rating) || !isset($status))
        {
            _MainModel::viewJSON("Error, you missed _GET");
        }
        elseif ($id == "" || $role == "" || $id_parent == "" || $rating == "" || $status == "")
        {
            _MainModel::viewJSON("Error, you missed something in string ");
        }
        else
        {
            _MainModel::table("user_cards")->add(array("id" => $id, "role" =>  $role
            ,"id_parent" =>  $id_parent, "rating" =>  $rating, "status" =>  $status))->send();
        }

    }
    public function PDOUserCards()
    {

        $stmt = self::$db->prepare("SELECT id FROM  user_cards WHERE role= :role AND status= :status");
        $result_query = $stmt->execute(array(":role" => _MainModel::$params_url['admin'], ":status" => _MainModel::$params_url['active']));

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        _MainModel::viewJSON($rows);

    }
    public function editUserCards()
    {
        _MainModel::table("user_cards")->edit(array("role" => "admin", "status" => "active"), array("id" => 1))->send();
    }
    public function deleteUserCards()
    {
        _MainModel::table("user_cards")->delete(array("id" => 2))->send();
    }
}