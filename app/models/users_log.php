<?php


class users_log extends _MainModel
{
    public function getListUsersLog(){
        $result = _MainModel::table("users_log")->get()->send();

        _MainModel::viewJSON($result);

    }
    public function AddUserLog()
    {

        $id_user = $_GET["id_user"];
        $timestamp = $_GET["timestamp"];
        $info = $_GET["info"];



        if (!isset($id_user) || !isset($timestamp) || !isset($info))
        {
            _MainModel::viewJSON("Error, you missed _GET");
        }
        elseif ($id_user == "" || $timestamp == "" || $info == "")
        {
            _MainModel::viewJSON("Error, you missed something in string ");
        }
        else
        {
            _MainModel::table("users_log")->add(array("id_user" => $id_user, "timestamp" =>  $timestamp
            ,"info" =>  $info))->send();
        }

    }
    public function PDOUserLog()
    {

        $stmt = self::$db->prepare("SELECT id_user FROM  users_log WHERE timestamp = :timestamp AND info= :info");
        $result_query = $stmt->execute(array(":timestamp" => _MainModel::$params_url['100'], ":info" => _MainModel::$params_url['none']));

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        _MainModel::viewJSON($rows);

    }
    public function editUserLog()
    {
        _MainModel::table("users_log")->edit(array("timestamp" => "150", "info" => "something"), array("id_user" => 1))->send();
    }
    public function deleteUserLog()
    {
        _MainModel::table("users_log")->delete(array("id_user" => 2))->send();
    }
}