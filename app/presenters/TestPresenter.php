<?php

/*
 * Every class derriving from Model has access to $this->db
 * $this->db is a PDO object
 * Has a config in /core/config/database.php
 * branch b1
 */

class TestPresenter extends _MainPresenter {

    /*User*/

    public function editUser()
    {
        echo (new Users())->editUser();
    }
    public function deleteUser()
    {
        echo (new Users())->deleteUser();
    }

    public function PDOUser()
    {
        echo (new Users())->PDOUser();

    }
	public function getListUsers(){
        echo (new Users())->getListUsers();
	}

    public function AddUser(){
        echo (new users())->AddUser();
    }

    /* User_cards */

    public function AddUserCards(){
        echo (new users_cards())->AddUserCards();
    }
    public function editUserCards()
    {
        echo (new Users_cards())->editUserCards();
    }
    public function deleteUserCards()
    {
        echo (new users_cards())->deleteUserCards();
    }

    public function PDOUserCards()
    {
        echo (new users_cards())->PDOUserCards();

    }
    public function getListUsersCards(){
        echo (new users_cards())->getListUsersCards();
    }

    /* Users_log */

    public function AddUserLog(){
        echo (new users_log())->AddUserLog();
    }
    public function editUserLog()
    {
        echo (new users_log())->editUserLog();
    }
    public function deleteUserLog()
    {
        echo (new users_log())->deleteUserLog();
    }

    public function PDOUserLog()
    {
        echo (new users_log())->PDOUserCLog();

    }
    public function getListUsersLog(){
        echo (new users_log())->getListUsersLog();
    }


	/* labels */
	public function rusLableTable(){ $this->renderLabel('rus', 'labelLayoutTable'); }

	public function engLableTable(){ $this->renderLabel('eng', 'labelLayoutTable'); }

}

?>